package modelweb;

public class mdlPickerDetail {
	public String IdPicker;
	public String IdCustomer;
	public String IdTransaction;
	public String Date;
	public String NamePicker;
	public String Phone;
	public String Image;
	public String Token;
	public String Status;
	
	public String getIdPicker() {
		return IdPicker;
	}
	public void setIdPicker(String idPicker) {
		IdPicker = idPicker;
	}
	
	public String getIdCustomer() {
		return IdCustomer;
	}
	public void setIdCustomer(String idCustomer) {
		IdCustomer = idCustomer;
	}
	public String getIdTransaction() {
		return IdTransaction;
	}
	public void setIdTransaction(String idTransaction) {
		IdTransaction = idTransaction;
	}
	
	
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getNamePicker() {
		return NamePicker;
	}
	public void setNamePicker(String namePicker) {
		NamePicker = namePicker;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getImage() {
		return Image;
	}
	public void setImage(String image) {
		Image = image;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	
	
}
