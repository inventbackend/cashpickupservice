package modelweb;

import java.util.List;

public class mdlFCMPicker {
	
	public String collapse_key;
	public Boolean delay_while_idle;
	public modelweb.mdlNotification notification;
	public modelweb.mdlDataPicker data;
	public int time_to_live; 
	public List<String> registration_ids;
	public String priority;
	
	
	
	public String getCollapse_key() {
		return collapse_key;
	}
	public void setCollapse_key(String collapse_key) {
		this.collapse_key = collapse_key;
	}
	public Boolean getDelay_while_idle() {
		return delay_while_idle;
	}
	public void setDelay_while_idle(Boolean delay_while_idle) {
		this.delay_while_idle = delay_while_idle;
	}
	public modelweb.mdlNotification getNotification() {
		return notification;
	}
	public void setNotification(modelweb.mdlNotification notification) {
		this.notification = notification;
	}
	public modelweb.mdlDataPicker getData() {
		return data;
	}
	public void setData(modelweb.mdlDataPicker data) {
		this.data = data;
	}
	public List<String> getRegistration_ids() {
		return registration_ids;
	}
	public void setRegistration_ids(List<String> registration_ids) {
		this.registration_ids = registration_ids;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public int getTime_to_live() {
		return time_to_live;
	}
	public void setTime_to_live(int time_to_live) {
		this.time_to_live = time_to_live;
	}
	
	
	
	
	
	
}
