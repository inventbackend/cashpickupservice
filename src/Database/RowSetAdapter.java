package Database;

import java.sql.SQLException;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

import org.eclipse.jdt.internal.compiler.ast.ThrowStatement;

public class RowSetAdapter {
	
	public static JdbcRowSet getJDBCRowSet() throws SQLException{
		
		//String databaseUrl = "jdbc:mysql://localhost:3306/pickup";
		String databaseUrl = "jdbc:mysql://localhost:3306/cashpickup";
	    String username = "sa";
	    String password = "cuki123";
		
		RowSetFactory rsFactory = RowSetProvider.newFactory();
		JdbcRowSet jRowSet = rsFactory.createJdbcRowSet();
		
		jRowSet.setUrl(databaseUrl);
		jRowSet.setUsername(username);
		jRowSet.setPassword(password);
		jRowSet.setReadOnly(false);
		
		return jRowSet;
	}
	
	public static void loadRowSet (JdbcRowSet jrs,String sql,model.mdlHistoryTransaction mdlParam) throws SQLException{
		
		jrs.setCommand(sql);
		jrs.execute();
	
	}
	
	

}
