package Database;

import java.sql.*;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

import javax.sql.rowset.*;

import org.joda.time.DateTime;

import javax.sql.DataSource;

import com.sun.org.apache.xerces.internal.impl.dv.xs.DateTimeDV;
import com.sun.rowset.JdbcRowSetImpl;

import javax.naming.*;




public class DataAdapter {
	
	
	
	public static String DatabaseAdapter(){
		
		String result = "";
		try {
			javax.naming.Context context =  new InitialContext();
			javax.naming.Context datasource = (javax.naming.Context) context.lookup("java:/comp/env");
			DataSource ds = (DataSource) datasource.lookup("jdbc/UsersDB");
			java.sql.Connection conn = ds.getConnection();
			
			java.sql.Statement statement = conn.createStatement();
			String sql = "SELECT `ID`,`Name` FROM test";
			ResultSet rs = statement.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
				
			
			int colNumber = rsmd.getColumnCount();
			while (rs.next()){
				for(int i = 1 ; i <= colNumber;i++){
					if(i>1) System.out.println(", ");
					String colValue = rs.getString(i);
//					System.out.print(colValue + " "+ rsmd.getColumnName(i));
					result = result + rs.getString(i);
				}
				System.out.println("");
			}
			
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (SQLException e){
			System.err.println(e);
		}
		
		return result;
	}
	
	public static String DBRowSet(){
		
		String result = "";
		String databaseUrl = "jdbc:mysql://localhost:3306/cashpickup";
	    String username = "sa";
	    String password = "cuki123";
	    JdbcRowSet rowSet = null;
	    
	    try{
	    	Class.forName("com.mysql.jdbc.Driver");
	    	
	    	
	    	rowSet = new JdbcRowSetImpl();
	    	rowSet.setUrl(databaseUrl);
	    	rowSet.setUsername(username);
	    	rowSet.setPassword(password);
	    	rowSet.setCommand("SELECT * FROM test");
	    	rowSet.setReadOnly(false);
	    	rowSet.execute();
	    	
	    	rowSet.beforeFirst();
	    	while(rowSet.next()){
	    		result = result + " "+ rowSet.getString("ID")+" "+rowSet.getString("Name");
	    	}
	    	
	    	rowSet.close();
	    	
	    }catch(SQLException ex){
	    	System.out.println(ex);
	    }catch(ClassNotFoundException ex){
	    	System.out.println(ex);
	    }
		
		
		
		return result;
	}
	
	public static String InsertTransaction(model.mdlHistoryTransaction json){
		String result = "";
		String databaseUrl = "jdbc:mysql://localhost:3306/cashpickup";
	    String username = "sa";
	    String password = "cuki123";
	    JdbcRowSet rowSet = null;
	    SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy hh:mm:ss");
	    
	    try{
	    	Class.forName("com.mysql.jdbc.Driver");
	    	
	    	
	    	rowSet = new JdbcRowSetImpl();
	    	rowSet.setUrl(databaseUrl);
	    	rowSet.setUsername(username);
	    	rowSet.setPassword(password);
	    	rowSet.setCommand("SELECT * FROM historytransaction");
	    	rowSet.setReadOnly(false);
	    	rowSet.execute();
	    	
	    	rowSet.moveToInsertRow();
	    	rowSet.updateString("IdHistory",json.getIdHistory());
	    	rowSet.updateString("Username",json.getUsername());
	    	rowSet.updateString("CodeBank",json.getCodeBank());
	    	rowSet.updateString("Date", json.getDate());
	    	rowSet.updateString("Address",json.getAddress());
	    	rowSet.updateString("AddressDetail", json.getAddressDetail());
	    	rowSet.updateString("Latitude",json.getLatitude());
	    	rowSet.updateString("Longitude", json.getLongitude());
	    	rowSet.updateString("Account",json.getAccount());
	    	rowSet.updateString("Nominal", json.getNominal());
	    	rowSet.updateString("Keterangan",json.getKeterangan());
	    	rowSet.updateString("Status", json.getStatus());
	    	rowSet.insertRow();
	    	
	    	result = "Insert Success..";
	    	
	    	rowSet.close();
	    	
	    }catch(SQLException ex){
	    	System.out.println(ex);
	    }catch(ClassNotFoundException ex){
	    	System.out.println(ex);
	    }
		
		
		
		return result;
	}
	


}
