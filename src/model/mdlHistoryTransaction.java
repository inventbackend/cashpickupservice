package model;


public class mdlHistoryTransaction {
	
	public String getIdHistory() {
		return IdHistory;
	}
	public void setIdHistory(String idHistory) {
		IdHistory = idHistory;
	}
	public String getCodeBank() {
		return CodeBank;
	}
	public void setCodeBank(String codeBank) {
		CodeBank = codeBank;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getAddressDetail() {
		return AddressDetail;
	}
	public void setAddressDetail(String addressDetail) {
		AddressDetail = addressDetail;
	}
	public String getLatitude() {
		return Latitude;
	}
	public void setLatitude(String latitude) {
		Latitude = latitude;
	}
	public String getLongitude() {
		return Longitude;
	}
	public void setLongitude(String longitude) {
		Longitude = longitude;
	}
	public String getAccount() {
		return Account;
	}
	public void setAccount(String account) {
		Account = account;
	}
	public String getNominal() {
		return Nominal;
	}
	public void setNominal(String nominal) {
		Nominal = nominal;
	}
	public String getKeterangan() {
		return Keterangan;
	}
	public void setKeterangan(String keterangan) {
		Keterangan = keterangan;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}



	public String IdHistory;
	public String CodeBank;
	public String Date;
	public String Address;
	public String AddressDetail;
	public String Latitude;
	public String Longitude;
	public String Account;
	public String Nominal;
	public String Keterangan;
	public String Status;	
	public String Token;
	public String Username;
	public String Password;
	public String Phone;
	

	

}
