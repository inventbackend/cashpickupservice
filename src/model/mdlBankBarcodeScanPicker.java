package model;

public class mdlBankBarcodeScanPicker {
	
	public String IdPicker;
	public String NamePicker;
	public String Phone;
	public String Image;
	
	public String getIdPicker() {
		return IdPicker;
	}
	public void setIdPicker(String idPicker) {
		IdPicker = idPicker;
	}
	public String getNamePicker() {
		return NamePicker;
	}
	public void setNamePicker(String namePicker) {
		NamePicker = namePicker;
	}
	
	
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getImage() {
		return Image;
	}
	public void setImage(String image) {
		Image = image;
	}
	
	
	
	

}
