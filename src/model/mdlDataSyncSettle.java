package model;

public class mdlDataSyncSettle {
	public String Title;
	public String IdTransaction;
	public String Status;
	public String IdPicker;
	
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getIdTransaction() {
		return IdTransaction;
	}
	public void setIdTransaction(String idTransaction) {
		IdTransaction = idTransaction;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getIdPicker() {
		return IdPicker;
	}
	public void setIdPicker(String idPicker) {
		IdPicker = idPicker;
	}
	
	
}
