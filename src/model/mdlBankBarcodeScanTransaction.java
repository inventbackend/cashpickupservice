package model;

public class mdlBankBarcodeScanTransaction {


	public String IdTransaction;
	public String IdPicker;
	public String IdCustomer;
	public String CustomerName;
	public String CodeBank;
	public String Address;
	public String AddressDetail;
	public String Phone;
	public String Date;
	public String Latitude;
	public String Longitude;
	public String AccountNo;
	public String Nominal;
	public String ActualNominal;
	public String Description;
	public String Barcode;
	public String Status;
	public String Token;
	
	public String NamePicker;
	public String PhonePicker;
	public String Image;
	
	public String getIdTransaction() {
		return IdTransaction;
	}
	public void setIdTransaction(String idTransaction) {
		IdTransaction = idTransaction;
	}
	public String getIdPicker() {
		return IdPicker;
	}
	public void setIdPicker(String idPicker) {
		IdPicker = idPicker;
	}
	public String getIdCustomer() {
		return IdCustomer;
	}
	public void setIdCustomer(String idCustomer) {
		IdCustomer = idCustomer;
	}
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getCodeBank() {
		return CodeBank;
	}
	public void setCodeBank(String codeBank) {
		CodeBank = codeBank;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getAddressDetail() {
		return AddressDetail;
	}
	public void setAddressDetail(String addressDetail) {
		AddressDetail = addressDetail;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getLatitude() {
		return Latitude;
	}
	public void setLatitude(String latitude) {
		Latitude = latitude;
	}
	public String getLongitude() {
		return Longitude;
	}
	public void setLongitude(String longitude) {
		Longitude = longitude;
	}
	public String getAccountNo() {
		return AccountNo;
	}
	public void setAccountNo(String accountNo) {
		AccountNo = accountNo;
	}
	public String getNominal() {
		return Nominal;
	}
	public void setNominal(String nominal) {
		Nominal = nominal;
	}
	
	public String getActualNominal() {
		return ActualNominal;
	}
	public void setActualNominal(String actualNominal) {
		ActualNominal = actualNominal;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	
	public String getBarcode() {
		return Barcode;
	}
	public void setBarcode(String barcode) {
		Barcode = barcode;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
	public String getNamePicker() {
		return NamePicker;
	}
	public void setNamePicker(String namePicker) {
		NamePicker = namePicker;
	}
	
	public String getPhonePicker() {
		return PhonePicker;
	}
	public void setPhonePicker(String phonePicker) {
		PhonePicker = phonePicker;
	}
	public String getImage() {
		return Image;
	}
	public void setImage(String image) {
		Image = image;
	}
	
	
	
	
	

}
