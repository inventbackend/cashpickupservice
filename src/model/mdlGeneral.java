package model;

import java.util.List;

public class mdlGeneral {
	
	public String Barcode;
	public String Status;
	public String IdTransaction;
	public String ActualAmount;
	public List<model.mdlFractionalCurrency> ListFractionalCurrency;
	
	public String getBarcode() {
		return Barcode;
	}
	public void setBarcode(String barcode) {
		Barcode = barcode;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getIdTransaction() {
		return IdTransaction;
	}
	public void setIdTransaction(String idTransaction) {
		IdTransaction = idTransaction;
	}
	public String getActualAmount() {
		return ActualAmount;
	}
	public void setActualAmount(String actualAmount) {
		ActualAmount = actualAmount;
	}
	public List<model.mdlFractionalCurrency> getListFractionalCurrency() {
		return ListFractionalCurrency;
	}
	public void setListFractionalCurrency(List<model.mdlFractionalCurrency> listFractionalCurrency) {
		ListFractionalCurrency = listFractionalCurrency;
	}
	
	
	
	
	
	
	

}
