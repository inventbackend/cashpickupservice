package model;

import java.util.List;

public class Data {

	public model.mdlData getNotification() {
		return notification;
	}
	public void setNotification(model.mdlData notification) {
		this.notification = notification;
	}
	public List<String> getRegistration_ids() {
		return registration_ids;
	}
	public void setRegistration_ids(List<String> registration_ids) {
		this.registration_ids = registration_ids;
	}
	public model.mdlData notification;
	public List<String> registration_ids;

}
