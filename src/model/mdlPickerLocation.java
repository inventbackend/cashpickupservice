package model;

public class mdlPickerLocation {
	public String IdTransaction;
	public String IdPicker;
	public String Longitude;
	public String Latitude;
	public String Date;
	public String getIdTransaction() {
		return IdTransaction;
	}
	public void setIdTransaction(String idTransaction) {
		IdTransaction = idTransaction;
	}
	public String getIdPicker() {
		return IdPicker;
	}
	public void setIdPicker(String idPicker) {
		IdPicker = idPicker;
	}
	public String getLongitude() {
		return Longitude;
	}
	public void setLongitude(String longitude) {
		Longitude = longitude;
	}
	public String getLatitude() {
		return Latitude;
	}
	public void setLatitude(String latitude) {
		Latitude = latitude;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	
	
	
	

}
