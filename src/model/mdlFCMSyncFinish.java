package model;

import java.util.List;

public class mdlFCMSyncFinish {
	public model.mdlNotification notification;
	public model.mdlDataSyncFinish data;
	public List<String> registration_ids;
	public model.mdlNotification getNotification() {
		return notification;
	}
	public void setNotification(model.mdlNotification notification) {
		this.notification = notification;
	}
	public model.mdlDataSyncFinish getData() {
		return data;
	}
	public void setData(model.mdlDataSyncFinish data) {
		this.data = data;
	}
	public List<String> getRegistration_ids() {
		return registration_ids;
	}
	public void setRegistration_ids(List<String> registration_ids) {
		this.registration_ids = registration_ids;
	}
	
	
}
