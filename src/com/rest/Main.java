package com.rest;

import java.awt.List;
import java.awt.PageAttributes.MediaType;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.rowset.JdbcRowSet;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.sun.jersey.api.container.MappableContainerException;

import model.mdlGetCurrency;
import model.mdlJSON;
import Database.DataAdapter;

@Path("/")
public class Main {
	
//	@GET
//	@Path("get1")
//	@Produces("application/json")
//	public mdlJSON text1()
//	{
//		mdlJSON model = new mdlJSON();
//		model.setId("122");
//		model.setName("NANDHA");
//		
//		return model;
//	}
	
	
	
	@POST
	@Path("login")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response login(model.mdlLogin param){
		
		model.mdlResult result = new model.mdlResult();
		result.setResult( adapter.LoginAdapter.login(param));

	    model.mdlResultList mdlResultList = new model.mdlResultList();
	    
	    java.util.List<model.mdlResult> mdlResult = new ArrayList<model.mdlResult>();
	    
	    mdlResult.add(result);
	    mdlResultList.setResultList(mdlResult);
	    
	    adapter.NotificationLogAdapter.LoginTriggerSendNotificationClient(param);
		
	    Gson gson = new Gson();
	    

	    String output = "";

	    output = gson.toJson(mdlResultList);
		
		
		return Response.status(201).entity(output).build();
	}
	
	@POST
	@Path("getHistoryTransaction")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response GetHistoryTransaction(model.mdlHistoryTransaction param) throws SQLException{
		String output = "";
		
		java.util.List<model.mdlHistoryTransaction> listTransaction = new ArrayList<model.mdlHistoryTransaction>();

	    listTransaction = adapter.HistoryTransactionAdapter.getHistoryTransaction(param);

	    
		Gson gson = new Gson();
		
		output = gson.toJson(listTransaction);

		return Response.status(201).entity(output).build();
	}
	
	@POST
	@Path("insertHistoryTransaction")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response InsertHistoryTransaction(model.mdlHistoryTransaction param){
		
	    model.mdlResultList mdlResultList = new model.mdlResultList();
	    
	    java.util.List<model.mdlResult> mdlResult = new ArrayList<model.mdlResult>();
	    
		model.mdlResult result =  new model.mdlResult();
		result.setResult(adapter.HistoryTransactionAdapter.InsertHistoryTransaction(param));
		mdlResult.add(result);
		
		result =  new model.mdlResult();
		result.setResult(adapter.HistoryTransactionAdapter.updateToken(param.getToken(), param.getUsername(), param.getPassword()));
		mdlResult.add(result);
		
		result =  new model.mdlResult();
		result.setResult(adapter.HistoryTransactionAdapter.UpdatePhone(param.getPhone(), param.getUsername()));
		mdlResult.add(result);
		
		result = new model.mdlResult();
		result.setResult(adapter.HistoryTransactionAdapter.SendStatusNotification(param.getUsername(), param.getPassword(),param.getStatus(),param.getIdHistory()));
		mdlResult.add(result);
		
		mdlResultList.setResultList(mdlResult);
		
		Gson gson = new Gson();
		    
	    String output = "";

	    output = gson.toJson(mdlResultList);

		return Response.status(201).entity(output).build();
		
		
		
	}
	
	
	@POST 
	@Path("getImage")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response GetImage(model.mdlGetImageReq param)
	{
		model.mdlGetImageResContainer mdlImageContainer = new model.mdlGetImageResContainer();
		
		model.mdlGetImageRes mdlImage = new model.mdlGetImageRes();
		mdlImage = adapter.ImageAdapter.GetImage(param);
		
		mdlImageContainer.setImageContainer(mdlImage);
		
		Gson gson = new Gson();
		
		String output ="";
		
		output = gson.toJson(mdlImageContainer);
		
		return Response.status(201).entity(output).build();
	}
	
	
	
	@POST 
	@Path("post")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response Test(model.mdlHistoryTransaction param)
	{
//		String result = "Data post: "+data.getId() + data.getName();
		model.mdlResult result = new model.mdlResult();
		
		
		result.setResult(DataAdapter.InsertTransaction(param));

	    model.mdlResultList mdlResultList = new model.mdlResultList();
	    
	    java.util.List<model.mdlResult> mdlResult = new ArrayList<model.mdlResult>();
	    
	    mdlResult.add(result);
	    mdlResultList.setResultList(mdlResult);
	    
	    Gson gson = new Gson();
		    

	    String output = "";

	    output = gson.toJson(mdlResultList);

		return Response.status(201).entity(output).build();
	
	}
	
	@POST
	@Path("syncStatusVerified")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response SyncStatusVerified(model.mdlGeneral param){
		
		String result = adapter.SyncStatusVerifiedAdapter.UpdateStatus(param);

		model.mdlGeneral mdlGeneral = new model.mdlGeneral();

	    
		Gson gson = new Gson();
	    
		String output ="";
		
		output = gson.toJson(mdlGeneral);
		
		adapter.SyncStatusVerifiedAdapter.SendStatusToPicker(param);
		String idPicker = adapter.SyncStatusVerifiedAdapter.getIdPickerByIdTransaction(param.getIdTransaction());
		adapter.NotificationConfirmationAdapter.InsertPickerNotificationLog(param.getIdTransaction(),idPicker,"CPVERIFIED");
		
		return Response.status(201).entity(output).build();
	}
	
	@POST
	@Path("notificationConfirmationClient")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response NotificationConfirmationClient(model.mdlNotificationLog param){
		
		model.mdlResult result = new model.mdlResult();
		result.setResult( adapter.NotificationLogAdapter.UpdateLogConfirmationClient(param));

	    model.mdlResultList mdlResultList = new model.mdlResultList();
	    
	    java.util.List<model.mdlResult> mdlResult = new ArrayList<model.mdlResult>();
	    
	    mdlResult.add(result);
	    mdlResultList.setResultList(mdlResult);
		
	    Gson gson = new Gson();
		
		String output ="";
		
		output = gson.toJson(mdlResultList);
		return Response.status(201).entity(output).build();
	}
	
	@POST
	@Path("setPickerRating")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response SetPickerRating(model.mdlRating param){
		
		model.mdlResult result = new model.mdlResult();
		result.setResult(adapter.RatingAdapter.UpdateTransactionRating(param));
		
		model.mdlResultList mdlResultList = new model.mdlResultList();
		
		java.util.List<model.mdlResult> mdlResult = new ArrayList<model.mdlResult>();
	    
	    mdlResult.add(result);
		mdlResultList.setResultList(mdlResult);
		
		Gson gson = new Gson();
			
		String output ="";
		
		output = gson.toJson(mdlResultList);
		return Response.status(201).entity(output).build();
	}
	
	@POST
	@Path("getpickerlocation")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response GetPickerLocation(model.mdlPickerLocation param){
		
		
		
		model.mdlPickerLocation mdlPickerLocation = new model.mdlPickerLocation();
		mdlPickerLocation = adapter.PickerLocationAdapter.GetPickerLocation(param);
	 
		
		Gson gson = new Gson();
		
		String output ="";
		
		output = gson.toJson(mdlPickerLocation);
		return Response.status(201).entity(output).build();
	}
	

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// PICKER FUNCTION
	
	
	
	@POST
	@Path("pickerLogin")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response PickerLogin(model.mdlLogin param){
		
		model.mdlResult result = new model.mdlResult();
		result.setResult( adapter.LoginAdapter.PickerLogin(param));

	    model.mdlResultList mdlResultList = new model.mdlResultList();
	    
	    java.util.List<model.mdlResult> mdlResult = new ArrayList<model.mdlResult>();
	    
	    mdlResult.add(result);
	    mdlResultList.setResultList(mdlResult);
		
	    adapter.NotificationLogAdapter.LoginTriggerSendNotificationPicker(param);
	    
	    Gson gson = new Gson();
	    

	    String output = "";

	    output = gson.toJson(mdlResultList);
		
		return Response.status(201).entity(output).build();
	}
	
	@POST
	@Path("syncStatusFinish")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response SyncStatusFinish(model.mdlGeneral param){
		
	
		String result = adapter.SyncStatusVerifiedAdapter.UpdateStatus(param);

		model.mdlGeneral mdlGeneral = new model.mdlGeneral();
	    
		Gson gson = new Gson();
	    
		String output ="";
		
		output = gson.toJson(mdlGeneral);
		
		String idPicker = adapter.SyncStatusVerifiedAdapter.getIdPickerByIdTransaction(param.getIdTransaction());
		adapter.SyncStatusFinishAdapter.SendStatusToClient(param,idPicker);
		String idClient = adapter.BarcodeGeneratorAdaptor.getIdCustomerByIdTransaction(param.getIdTransaction());
		adapter.NotificationConfirmationAdapter.InsertClientNotificationLog(param.getIdTransaction(),idClient, "CPFINISH");
		
		return Response.status(201).entity(output).build();
	}
	
	@POST 
	@Path("getBarcode")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response GetBarcode(model.mdlGeneral param)
	{
		model.mdlGeneralContainer mdlBarcodeContainer = new model.mdlGeneralContainer();
		model.mdlGeneral mdlBarcode = new model.mdlGeneral();
		
		mdlBarcode = adapter.BarcodeGeneratorAdaptor.GenerateBarcode(param);
		mdlBarcode.setListFractionalCurrency(param.getListFractionalCurrency()); 
		
		String insertFractionalCurrency = adapter.BarcodeGeneratorAdaptor.InsertFractionalCurrency(param);
		
		mdlBarcodeContainer.setGeneralContainer(mdlBarcode);
		
		Gson gson = new Gson();
		
		String output ="";
		
		output = gson.toJson(mdlBarcodeContainer);
		
		adapter.BarcodeGeneratorAdaptor.SendBarcodeToClient(mdlBarcode);
		String idCustomer = adapter.BarcodeGeneratorAdaptor.getIdCustomerByIdTransaction(param.getIdTransaction());
		adapter.NotificationConfirmationAdapter.InsertClientNotificationLog(param.getIdTransaction(),idCustomer, "CPBARCODE");
		
		return Response.status(201).entity(output).build();
	}
	
	
	@POST
	@Path("notificationConfirmationPicker")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response NotificationConfirmationPicker(model.mdlNotificationLog param){
		
		model.mdlResult result = new model.mdlResult();
		result.setResult( adapter.NotificationLogAdapter.UpdateLogConfirmationPicker(param));

	    model.mdlResultList mdlResultList = new model.mdlResultList();
	    
	    java.util.List<model.mdlResult> mdlResult = new ArrayList<model.mdlResult>();
	    
	    mdlResult.add(result);
	    mdlResultList.setResultList(mdlResult);
		
	    Gson gson = new Gson();
		
		String output ="";
		
		output = gson.toJson(mdlResultList);
		return Response.status(201).entity(output).build();
	}
	
	@POST
	@Path("insertpickerlocation")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response InsertPickerLocation(model.mdlPickerLocation param){
		
		
		model.mdlResult result = new model.mdlResult();
		result.setResult(adapter.PickerLocationAdapter.InsertPickerLocation(param));
		
		model.mdlResultList mdlResultList = new model.mdlResultList();
		    
	    java.util.List<model.mdlResult> mdlResult = new ArrayList<model.mdlResult>();
	    
	    mdlResult.add(result);
	    mdlResultList.setResultList(mdlResult);
		    
		
		Gson gson = new Gson();
		
		String output ="";
		
		output = gson.toJson(mdlResultList);
		return Response.status(201).entity(output).build();
	}
	
	@POST
	@Path("getfractionalcurrency")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response getFractionalCurrency(){
		java.util.List<mdlGetCurrency> listCurrency = new java.util.ArrayList<mdlGetCurrency>();
		
		listCurrency.addAll(adapter.FractionalCurrencyFacade.GetFractionalCurrency());
		
		Gson gson = new Gson();
		
		String output ="";
		
		output = gson.toJson(listCurrency);
		return Response.status(201).entity(output).build();
	
	}
	
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//SETTLEMENT
	
	@POST
	@Path("bankLogin")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response LoginBank (model.mdlLogin param){
		
		model.mdlResult result = new model.mdlResult();
		result.setResult(adapter.LoginAdapter.BankLogin(param));

	    model.mdlResultList mdlResultList = new model.mdlResultList();
	    
	    java.util.List<model.mdlResult> mdlResult = new ArrayList<model.mdlResult>();
	    
	    mdlResult.add(result);
	    mdlResultList.setResultList(mdlResult);
	    
	    Gson gson = new Gson();

	    String output = "";

	    output = gson.toJson(mdlResultList);
		
		return Response.status(201).entity(output).build();
	}
	
	@POST
	@Path("bankScanBarcode")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response BankScanBarcode(model.mdlGeneral param){
		
		model.mdlBankBarcodeScan mdlContainer = new model.mdlBankBarcodeScan();
		model.mdlBankBarcodeScanTransaction mdlBankBarcodeScan = new model.mdlBankBarcodeScanTransaction();
		mdlBankBarcodeScan = adapter.bankScanBarcodeAdapter.getOrderDetail(param);
		
		mdlContainer.setBankTransaction(mdlBankBarcodeScan);
		Gson gson = new Gson();
		
	    String output = "";

	    output = gson.toJson(mdlContainer);
		
		return Response.status(201).entity(output).build();
	}
	
	@POST
	@Path("syncStatusSettle")
	@Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public Response SyncSettle(model.mdlGeneral param){
		
		String result = adapter.SyncStatusSettleAdapter.UpdateStatusSettle(param);

		model.mdlGeneral mdlGeneral = new model.mdlGeneral();
	    
		Gson gson = new Gson();
	    
		String output ="";
		
		output = gson.toJson(mdlGeneral);
		
		adapter.SyncStatusSettleAdapter.SendStatusToPicker(param);
		String idPicker = adapter.SyncStatusVerifiedAdapter.getIdPickerByIdTransaction(param.getIdTransaction());
		adapter.NotificationConfirmationAdapter.InsertPickerNotificationLog(param.getIdTransaction(),idPicker, "CPSETTLE");
		
		adapter.SyncStatusSettleAdapter.SendStatusToClient(param,idPicker);
		String idClient = adapter.BarcodeGeneratorAdaptor.getIdCustomerByIdTransaction(param.getIdTransaction());
		adapter.NotificationConfirmationAdapter.InsertClientNotificationLog(param.getIdTransaction(),idClient, "CPSETTLE");

		return Response.status(201).entity(output).build();
	}
	
	
	
	
	
	

}
