package adapter;

import java.sql.SQLException;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

public class bankScanBarcodeAdapter {
	
	public static model.mdlBankBarcodeScanTransaction getOrderDetail(model.mdlGeneral param){

		model.mdlBankBarcodeScanTransaction mdlOrderDetail = new model.mdlBankBarcodeScanTransaction();

		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT a.IdHistory,b.IdPicker,a.Username,a.CodeBank,a.Address,a.AddressDetail,d.Phone,a.Date,a.Latitude,a.Longitude,a.Account,a.Nominal,a.ActualNominal,a.Keterangan,a.Barcode,a.`Status`,c.Token,d.Username as UsernamePicker ,d.FullName,d.Phone as PhonePicker,d.Image, e.Username as UsernameClient, e.CustomerName,e.Phone as PhoneClient FROM historytransaction a INNER JOIN orderpicker b ON a.IdHistory = b.IdHistory INNER JOIN pickeruser c ON b.IdPicker = c.Username INNER JOIN employee d ON c.Username = d.Username INNER JOIN customer e ON a.Username = e.Username WHERE a.Barcode = ?");
			jrs.setString(1, param.getBarcode());
			jrs.execute();
			
			while(jrs.next()){
				mdlOrderDetail.setIdTransaction(jrs.getString("IdHistory"));
				mdlOrderDetail.setIdPicker(jrs.getString("IdPicker"));
				mdlOrderDetail.setIdCustomer(jrs.getString("Username"));
				mdlOrderDetail.setCustomerName(jrs.getString("CustomerName"));
				mdlOrderDetail.setCodeBank(jrs.getString("CodeBank"));
				mdlOrderDetail.setAddress(jrs.getString("Address"));
				mdlOrderDetail.setAddressDetail(jrs.getString("AddressDetail"));
				mdlOrderDetail.setPhone(jrs.getString("Phone"));
				mdlOrderDetail.setDate(jrs.getString("Date"));
				mdlOrderDetail.setLatitude(jrs.getString("Latitude"));
				mdlOrderDetail.setLongitude(jrs.getString("Longitude"));
				mdlOrderDetail.setAccountNo(jrs.getString("Account"));
				mdlOrderDetail.setNominal(jrs.getString("Nominal"));
				mdlOrderDetail.setActualNominal(jrs.getString("ActualNominal"));
				mdlOrderDetail.setDescription(jrs.getString("Keterangan"));
				mdlOrderDetail.setBarcode(jrs.getString("Barcode"));
				mdlOrderDetail.setStatus(jrs.getString("Status"));
				mdlOrderDetail.setToken(jrs.getString("Token"));
				mdlOrderDetail.setNamePicker(jrs.getString("FullName"));
				mdlOrderDetail.setPhonePicker(jrs.getString("PhonePicker"));
				mdlOrderDetail.setImage(jrs.getString("Image"));
				
			}
			
			
			
			jrs.close();
			
			
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return mdlOrderDetail;
	}

}
