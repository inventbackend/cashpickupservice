package adapter;

import java.sql.SQLException;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

public class LoginAdapter {
	
	public static String login(model.mdlLogin param){
		
		String result = "";
		
		if(param.getToken() == null){
			
			result = "2";
			return result;
		}
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM user where Username = ? AND Password = ?");
			jrs.setString(1,param.getUsername());
			jrs.setString(2, param.getPassword());
			jrs.execute();
			
			
			
			model.mdlLogin mdlLogin = new model.mdlLogin();
			while(jrs.next()){
				mdlLogin.setUsername(jrs.getString("Username"));
				mdlLogin.setPassword(jrs.getString("Password"));
				mdlLogin.setToken("Token");
			}
			
			
			
			if( mdlLogin.getUsername() != null ) {
				jrs.last();
				int rowNum = jrs.getRow();
				
				jrs.absolute(rowNum);
				jrs.updateString("Token", param.getToken());
				jrs.updateRow();
				jrs.close();
				result ="1";
				
			}else{
				jrs.close();
				
				result = "0";
			}
			
			
			
			
			
			
			
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return result;
		
	}
	
	public static String PickerLogin(model.mdlLogin param){
		
		String result = "";
		if(param.getToken() == null){
			result = "2";
			return result;
		}
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM pickeruser where Username = ? AND Password = ?");
			jrs.setString(1,param.getUsername());
			jrs.setString(2, param.getPassword());
			jrs.execute();
			
			int intRow = jrs.getRow();
			
			
			
			
			model.mdlLogin mdlLogin = new model.mdlLogin();
			while(jrs.next()){
				mdlLogin.setUsername(jrs.getString("Username"));
				mdlLogin.setPassword(jrs.getString("Password"));
				mdlLogin.setToken("Token");
			}
			
			
			
			if( mdlLogin.getUsername() != null ) {
				jrs.last();
				int rowNum = jrs.getRow();
				
				jrs.absolute(rowNum);
				jrs.updateString("Token", param.getToken());
				jrs.updateRow();
				jrs.close();
				result ="1";
				
			}else{
				
				result = "0";
			}
			
			
			
			
			
			
			
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return result;
		
	}
	
	public static String BankLogin(model.mdlLogin param){
		
		String result = "";
		if(param.getToken() == null){
			result = "2";
			return result;
		}
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM bankuser where Username = ? AND Password = ?");
			jrs.setString(1,param.getUsername());
			jrs.setString(2, param.getPassword());
			jrs.execute();
			
			int intRow = jrs.getRow();
			
			
			
			
			model.mdlLogin mdlLogin = new model.mdlLogin();
			while(jrs.next()){
				mdlLogin.setUsername(jrs.getString("Username"));
				mdlLogin.setPassword(jrs.getString("Password"));
				mdlLogin.setToken("Token");
			}
			
			
			
			if( mdlLogin.getUsername() != null ) {
				jrs.last();
				int rowNum = jrs.getRow();
				
				jrs.absolute(rowNum);
				jrs.updateString("Token", param.getToken());
				jrs.updateRow();
				jrs.close();
				result ="1";
				
			}else{
				
				result = "0";
			}
			
			
			
			
			
			
			
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return result;
		
	}


}
