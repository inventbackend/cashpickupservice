package adapter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

public class SyncStatusSettleAdapter {
	
	public static String UpdateStatusSettle(model.mdlGeneral param){
		String result = "";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM historytransaction where IdHistory = ?");
			jrs.setString(1,param.getIdTransaction());
		
			jrs.execute();
			
		
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("Status", "SETTLE");
			jrs.updateRow();
			jrs.close();
			result ="1";
				
		
			
			
			
			
			
			
			
		}catch(SQLException ex){
			result = "0";
		}catch(ClassNotFoundException ex){
			result = "0";
		}
		
		return result;
	}
	public static void SendStatusToClient(model.mdlGeneral param,String idPicker){
		String message = "Your Transaction Has Been Settle.";
		String title = "CPSETTLE";
		
		List<String> listRegId = new ArrayList<String>();
		
		listRegId.add(adapter.TokenAdapter.GetClientTokenByIdTransaction(param.getIdTransaction()));
		
		
		model.mdlDataSyncSettle data = new model.mdlDataSyncSettle();
		data.setTitle(title);
		data.setIdTransaction(param.getIdTransaction());
		data.setIdPicker(idPicker);
		data.setStatus(param.getStatus());
		
		adapter.SyncStatusSettleAdapter.SendNotificationSyncSettleToClient(message, title, listRegId, data);
	}
	public static void SendStatusToPicker(model.mdlGeneral param){
		
		String message = "Settlement Succeed";
		String title = "CPSETTLE";
		
		List<String> listRegId = new ArrayList<String>();
		
		listRegId.add(adapter.TokenAdapter.GetPickerTokenByIdTransaction(param.getIdTransaction()));
		
		
		model.mdlDataGeneral data = new model.mdlDataGeneral();
		data.setTitle(title);
		data.setIdTransaction(param.getIdTransaction());
		data.setStatus(param.getStatus());
		
		adapter.PushNotificationAdapter.SendNotificationGeneralToPicker(message, title, listRegId, data);
	}
	public static void SendNotificationSyncSettleToClient(String message,String title,List<String> listRegId, model.mdlDataSyncSettle data){
		System.out.println( "Sending POST to GCM" );

        String apiKey = "AIzaSyDRphEly0JYuhRRqjZ-49eiX6yT5cvoYDM";
        //Content content = createContent();
        
        model.mdlFCMSyncSettle content = new model.mdlFCMSyncSettle();
        model.mdlNotification notif = new model.mdlNotification();
        List<String> regid = new ArrayList<String>();
        
        notif.setText(message);
        notif.setTitle(title);
        
        for(String temp : listRegId){
        	regid.add(temp);
        }
        	

        content.setNotification(notif);
        content.setData(data);
        content.setRegistration_ids(regid);
        
        adapter.PushNotificationAdapter.PostToGCMSyncSettle(apiKey, content);
	}
}
