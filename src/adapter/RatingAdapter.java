package adapter;

import java.sql.SQLException;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

public class RatingAdapter {
	
	public static String UpdateTransactionRating(model.mdlRating param){
		
		String result = "";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM historytransaction where IdHistory = ?");
			jrs.setString(1,param.getIdTransaction());
		
			jrs.execute();
			
		
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("Comment", param.getComment());
			jrs.updateString("Rating", param.getRating());
			jrs.updateRow();
			
			
			
			jrs.close();
			result ="1";
				
		}catch(SQLException ex){
			result = "0";
		}catch(ClassNotFoundException ex){
			result = "0";
		}
		
		return result;
	}
}
