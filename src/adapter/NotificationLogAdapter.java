package adapter;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.google.gson.Gson;
import com.sun.rowset.JdbcRowSetImpl;

public class NotificationLogAdapter {
	
	
	public static model.mdlGeneral GetBarcode(String idHistory){
		model.mdlGeneral mdlBarcode = new model.mdlGeneral();
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM historytransaction WHERE IdHistory = ?");
			jrs.setString(1,idHistory);
		
			jrs.execute();
			
			
			
			
			while(jrs.next()){
				mdlBarcode.setIdTransaction(idHistory);
				mdlBarcode.setBarcode(jrs.getString("Barcode"));
				mdlBarcode.setActualAmount(jrs.getString("ActualNominal"));
			}
		
		
			
			jrs.close();
			
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return mdlBarcode;
	}
	
	
	public static String UpdateLogConfirmationPicker(model.mdlNotificationLog param){
		
		String result = "0";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM notificationlog where IdHistory = ? AND IdPicker = ? AND Title = ? ");
			jrs.setString(1,param.getIdTransaction());
			jrs.setString(2, param.getUsername());
			jrs.setString(3, param.getTitle());
			jrs.execute();
			
			
			
		
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateInt("Recieved", 1);
			jrs.updateRow();
			jrs.close();
			result = "1";
			
		}catch(SQLException ex){
			System.out.println(ex);
			
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		return result;
	}
	
	public static String UpdateLogConfirmationClient(model.mdlNotificationLog param){
		
		String result = "0";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM notificationlogclient where IdHistory = ? AND Username = ? AND Title = ?");
			jrs.setString(1,param.getIdTransaction());
			jrs.setString(2,param.getUsername());
			jrs.setString(3,param.getTitle());
			
			jrs.execute();
			
			
			
		
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateInt("Recieved", 1);
			jrs.updateRow();
			jrs.close();
			result = "1";
			
		}catch(SQLException ex){
			System.out.println(ex);
			
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		return result;
	}
	
	
	
	public static String RefreshBarcodeSendNotification(model.mdlGeneral param){
		String result ="1";
		String idPicker = adapter.BarcodeGeneratorAdaptor.getIdPickerByIdTransaction(param.getIdTransaction());
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM notificationlog where IdPicker = ? AND Recieved = 0");
			jrs.setString(1,idPicker);
			jrs.execute();
			
			int intRow = jrs.getRow();
			
			
			
			
			List<model.mdlNotificationLog> listNotif = new ArrayList<model.mdlNotificationLog>();
			
			while(jrs.next()){
				model.mdlNotificationLog mdlNotif = new model.mdlNotificationLog();
				mdlNotif.setUsername(jrs.getString("IdPicker"));
				mdlNotif.setIdTransaction(jrs.getString("IdHistory"));
				mdlNotif.setRecieved(jrs.getString("Recieved"));
				listNotif.add(mdlNotif);
				
			}
			
			
			for(model.mdlNotificationLog temp : listNotif){
				
				adapter.NotificationLogAdapter.SendPushNotificationPicker(temp.getIdTransaction(), temp.getUsername());
			}
			
			result = "1";
			
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return result;
	}
	
	public static String LoginTriggerSendNotificationPicker(model.mdlLogin param){
		
		String result ="1";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM notificationlog where IdPicker = ? AND Recieved = 0");
			jrs.setString(1,param.getUsername());
			jrs.execute();
			
			int intRow = jrs.getRow();
			
			
			
			
			List<model.mdlNotificationLog> listNotif = new ArrayList<model.mdlNotificationLog>();
			
			while(jrs.next()){
				model.mdlNotificationLog mdlNotif = new model.mdlNotificationLog();
				mdlNotif.setUsername(jrs.getString("IdPicker"));
				mdlNotif.setIdTransaction(jrs.getString("IdHistory"));
				mdlNotif.setTitle(jrs.getString("Title"));
				mdlNotif.setRecieved(jrs.getString("Recieved"));
				listNotif.add(mdlNotif);
				
			}
			
			
			for(model.mdlNotificationLog temp : listNotif){
				
				if("CPACCEPTED".equals(temp.getTitle())){
					adapter.NotificationLogAdapter.SendPushNotificationPicker(temp.getIdTransaction(), temp.getUsername());
				}else if("CPVERIFIED".equals(temp.getTitle())){
					
					model.mdlGeneral mdlGeneral = new model.mdlGeneral();
					mdlGeneral.setIdTransaction(temp.getIdTransaction());
					mdlGeneral.setStatus("VERIFIED");

					adapter.SyncStatusVerifiedAdapter.SendStatusToPicker(mdlGeneral);
				}else if("CPSETTLE".equals(temp.getTitle())){
					
					model.mdlGeneral mdlGeneral = new model.mdlGeneral();
					mdlGeneral.setIdTransaction(temp.getIdTransaction());
					mdlGeneral.setStatus("SETTLE");
					
					adapter.SyncStatusSettleAdapter.SendStatusToPicker(mdlGeneral);
				}
				
			}
			
			result = "1";
			
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return result;
	}
	public static String LoginTriggerSendNotificationClient(model.mdlLogin param){
		
		String result ="1";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM notificationlogclient where Username = ? AND Recieved = 0");
			jrs.setString(1,param.getUsername());
			jrs.execute();
			
			int intRow = jrs.getRow();
			
			
			
			
			List<model.mdlNotificationLog> listNotif = new ArrayList<model.mdlNotificationLog>();
			
			while(jrs.next()){
				model.mdlNotificationLog mdlNotif = new model.mdlNotificationLog();
				mdlNotif.setUsername(jrs.getString("Username"));
				mdlNotif.setIdTransaction(jrs.getString("IdHistory"));
				mdlNotif.setTitle(jrs.getString("Title"));
				mdlNotif.setRecieved(jrs.getString("Recieved"));
				listNotif.add(mdlNotif);
				
			}
			
			
			for(model.mdlNotificationLog temp : listNotif){
				
				if("CPIMAGE".equals(temp.getTitle())){
					adapter.NotificationLogAdapter.SendPushNotificationClient(temp.getIdTransaction());
				}else if("CPBARCODE".equals(temp.getTitle())){
					
					model.mdlGeneral mdlGeneral = new model.mdlGeneral();
					mdlGeneral = GetBarcode(temp.getIdTransaction());
					adapter.BarcodeGeneratorAdaptor.SendBarcodeToClient(mdlGeneral);
				}else if("CPFINISH".equals(temp.getTitle())){
					model.mdlGeneral mdlGeneral = new model.mdlGeneral();
					mdlGeneral.setIdTransaction(temp.getIdTransaction());
					mdlGeneral.setStatus("FINISH");
					
					String idPicker = adapter.SyncStatusVerifiedAdapter.getIdPickerByIdTransaction(temp.getIdTransaction());
					adapter.SyncStatusFinishAdapter.SendStatusToClient(mdlGeneral,idPicker);
				}else if("CPSETTLE".equals(temp.getTitle())){
					
					model.mdlGeneral mdlGeneral = new model.mdlGeneral();
					mdlGeneral.setIdTransaction(temp.getIdTransaction());
					mdlGeneral.setStatus("SETTLE");
					
					String idPicker = adapter.SyncStatusVerifiedAdapter.getIdPickerByIdTransaction(temp.getIdTransaction());
					adapter.SyncStatusSettleAdapter.SendStatusToClient(mdlGeneral,idPicker);
				}
				
			}
			
			result = "1";
			
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return result;
	}

	public static void SendPushNotificationPicker(String idHistory, String idPicker){
		String message = "One Order Found You.";
		String title = "CPACCEPTED";
		
		List<String> listRegId = new ArrayList<String>();
		
		modelweb.mdlOrderDetail orderDetail = new modelweb.mdlOrderDetail();
		orderDetail = GetOrderDataToPicker(idHistory);
		
		
		listRegId.add(orderDetail.getToken());
		
		
		modelweb.mdlDataPicker data = new modelweb.mdlDataPicker();
		
		data.setTitle(title);
		data.setIdTransaction(orderDetail.getIdTransaction());
		data.setIdCustomer(orderDetail.getIdCustomer());
		data.setCustomerName(orderDetail.getCustomerName());
		data.setCodeBank(orderDetail.getCodeBank());
		data.setAddress(orderDetail.getCodeBank());
		data.setAddressDetail(orderDetail.getAddressDetail());
		data.setPhone(orderDetail.getPhone());
		data.setDate(orderDetail.getDate());
		data.setLatitude(orderDetail.getLatitude());
		data.setLongitude(orderDetail.getLongitude());
		data.setAccountNo(orderDetail.getAccountNo());
		data.setNominal(orderDetail.getNominal());
		data.setDescription(orderDetail.getDescription());
		data.setStatus(orderDetail.getStatus());
//		data.setBarcode(orderDetail.getIdTransaction() + orderDetail.getIdCustomer() + adapter.VariableAdapter.ChangeDateFormat("yyyy-M-dd hh:mm:ss", "yyyyMddhhmmss", orderDetail.getDate()));
		
		
		adapter.PushNotificationBankAdapter.SendNotificationPicker(message, title, listRegId, data);
//		adapter.NotificationConfirmationAdapter.InsertPickerNotificationLog(idHistory,idPicker,title);
		
		
		
	}
	public static void SendPushNotificationClient(String idHistory){
		
		String message = "Your Cash Picker Is Ready.";
		String title = "CPIMAGE";
		
		List<String> listRegId = new ArrayList<String>();
		
		modelweb.mdlPickerDetail pickerDetail = new modelweb.mdlPickerDetail();
		pickerDetail = GetPickerDetailToClient(idHistory);
		
		listRegId.add(pickerDetail.getToken());
		
		modelweb.mdlDataClient data = new modelweb.mdlDataClient();
		
		data.setTitle(title);
		data.setIdTransaction(pickerDetail.getIdTransaction());
		data.setIdPicker(pickerDetail.getIdPicker());
		data.setNamePicker(pickerDetail.getNamePicker());
		data.setPhone(pickerDetail.getPhone());
//		data.setImage(pickerDetail.getImage());
		data.setStatus(pickerDetail.getStatus());
//		data.setBarcode(pickerDetail.getIdTransaction() + pickerDetail.getIdCustomer() + adapter.VariableAdapter.ChangeDateFormat("yyyy-M-dd hh:mm:ss", "yyyyMddhhmmss", pickerDetail.getDate()));
		
		adapter.NotificationLogAdapter.SendNotificationClient(message, title, listRegId, data);
		
		//String idClient = adapter.BarcodeGeneratorAdaptor.getIdCustomerByIdTransaction(idHistory);
		//adapter.NotificationConfirmationAdapter.InsertClientNotificationLog(idHistory, idClient, title);
	}

	public static modelweb.mdlOrderDetail GetOrderDataToPicker(String idHistory){
		modelweb.mdlOrderDetail orderData = new modelweb.mdlOrderDetail();
		
		try{
			
		
			
			
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT a.IdHistory,a.Username,e.CustomerName,a.CodeBank,a.Address,a.AddressDetail,d.Phone,a.Date,a.Latitude,a.Longitude,a.Account,a.Nominal,a.Keterangan,a.`Status`,c.Token FROM historytransaction a INNER JOIN orderpicker b ON a.IdHistory = b.IdHistory INNER JOIN pickeruser c ON b.IdPicker = c.Username INNER JOIN employee d ON c.Username = d.Username INNER JOIN customer e ON a.Username = e.Username WHERE a.IdHistory = ? ");
			jrs.setString(1, idHistory);
			jrs.execute();
			
			
			
			
			while(jrs.next()){
				
				
				orderData.setIdTransaction(jrs.getString("IdHistory"));
				orderData.setIdCustomer(jrs.getString("Username"));
				orderData.setCustomerName(jrs.getString("CustomerName"));
				orderData.setCodeBank(jrs.getString("CodeBank"));
				orderData.setAddress(jrs.getString("Address"));
				orderData.setAddressDetail(jrs.getString("AddressDetail"));
				orderData.setPhone(jrs.getString("Phone"));
				orderData.setDate(jrs.getString("Date"));
				orderData.setLatitude(jrs.getString("Latitude"));
				orderData.setLongitude(jrs.getString("Longitude"));
				orderData.setAccountNo(jrs.getString("Account"));
				orderData.setNominal(jrs.getString("Nominal"));
				orderData.setDescription(jrs.getString("Keterangan"));
				orderData.setStatus(jrs.getString("Status"));
				orderData.setToken(jrs.getString("Token"));

	
			}
			
			
			
			jrs.close();
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return orderData;
	}
	
	public static modelweb.mdlPickerDetail GetPickerDetailToClient(String idHistory){
		modelweb.mdlPickerDetail clientData = new modelweb.mdlPickerDetail();
		
		try{
			
		
			
			
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("Select a.IdHistory,a.Date,a.Username as IdCustomer,b.Token,d.Username,d.FullName,d.Phone,d.Image,a.Status FROM historytransaction a INNER JOIN `user` b ON a.Username = b.Username INNER JOIN orderpicker c ON a.IdHistory = c.IdHistory INNER JOIN employee d ON c.IdPicker = d.Username WHERE a.IdHistory = ?");
			jrs.setString(1, idHistory);
			jrs.execute();
			
			
			
			
			while(jrs.next()){
				
				
				clientData.setIdTransaction(jrs.getString("IdHistory"));
				clientData.setIdPicker(jrs.getString("Username"));
				clientData.setIdCustomer(jrs.getString("IdCustomer"));
				clientData.setDate(jrs.getString("Date"));
				clientData.setToken(jrs.getString("Token"));
				clientData.setIdPicker(jrs.getString("Username"));
				clientData.setNamePicker(jrs.getString("FullName"));
				clientData.setPhone(jrs.getString("Phone"));
//				clientData.setImage(jrs.getString("Image"));
				clientData.setStatus(jrs.getString("Status"));
	
			}
			
			
			
			jrs.close();
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return clientData;
	}
	public static void SendNotificationClient(String message,String title,List<String> listRegId,modelweb.mdlDataClient data){
		System.out.println( "Sending POST to GCM" );

        String apiKey = "AIzaSyDRphEly0JYuhRRqjZ-49eiX6yT5cvoYDM";
        //Content content = createContent();
        
        modelweb.mdlFCMClient content = new modelweb.mdlFCMClient();
        modelweb.mdlNotification notif = new modelweb.mdlNotification();
        notif.setText(message);
        notif.setTitle(title);
        notif.setClick_action("ACTIVITY_ACCEPTED");
        
        
        List<String> regid = new ArrayList<String>();
        
       
        
        for(String temp : listRegId){
        	regid.add(temp);
        }
        	
        
        content.setCollapse_key("CashPickupClient");
        content.setDelay_while_idle(true);
        content.setTime_to_live(360);
        content.setPriority("high");
        content.setNotification(notif);
        content.setData(data);
        content.setRegistration_ids(regid);
        
        adapter.NotificationLogAdapter.PostToGCMClient(apiKey, content);
	}
	
	public static void PostToGCMClient(String apiKey, modelweb.mdlFCMClient content){
		try{

	        // 1. URL
	        URL url = new URL("https://fcm.googleapis.com/fcm/send");

	        // 2. Open connection
	        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

	        // 3. Specify POST method
	        conn.setRequestMethod("POST");

	        // 4. Set the headers
	        conn.setRequestProperty("Content-Type", "application/json");
	        conn.setRequestProperty("Authorization", "key="+apiKey);

	        conn.setDoOutput(true);
	        
	        Gson gson = new Gson();
	        
	        String json = gson.toJson(content);
	        
	        System.out.println(json);
	        

	            // 5. Add JSON data into POST request body

	            //`5.1 Use Jackson object mapper to convert Contnet object into JSON

	            // 5.2 Get connection output stream
	            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());

	            // 5.3 Copy Content "JSON" into
	            //mapper.writeValue(wr, content);
	            wr.write(json.getBytes("UTF-8"));
	            

	            // 5.4 Send the request
	            wr.flush();

	            // 5.5 close
	            wr.close();

	            // 6. Get the response
	            int responseCode = conn.getResponseCode();
	            System.out.println("\nSending 'POST' request to URL : " + url);
	            System.out.println("Response Code : " + responseCode);

	            BufferedReader in = new BufferedReader(
	                    new InputStreamReader(conn.getInputStream()));
	            String inputLine;
	            StringBuffer response = new StringBuffer();

	            while ((inputLine = in.readLine()) != null) {
	                response.append(inputLine);
	            }
	            in.close();

	            // 7. Print result
	            System.out.println(response.toString());

	            } catch (MalformedURLException e) {
	                e.printStackTrace();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
		
	}
}
