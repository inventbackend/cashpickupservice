package adapter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import org.apache.jasper.tagplugins.jstl.core.ForEach;

import com.sun.rowset.JdbcRowSetImpl;

public class BarcodeGeneratorAdaptor {
	
	public static String getIdPickerByIdTransaction(String idTransaction){
		String idPicker ="";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT IdPicker FROM orderpicker WHERE IdHistory = ?");
			jrs.setString(1,idTransaction);
		
			jrs.execute();
			
			
			while(jrs.next()){
				idPicker = jrs.getString("IdPicker");
			}
			
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return idPicker;
	}
	public static String getIdCustomerByIdTransaction(String idTransaction){
		String idCustomer ="";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT Username FROM historytransaction WHERE IdHistory = ?");
			jrs.setString(1,idTransaction);
		
			jrs.execute();
			
			
			while(jrs.next()){
				idCustomer = jrs.getString("Username");
			}
			
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return idCustomer;
	}
	
	public static model.mdlGeneral GenerateBarcode(model.mdlGeneral param){
		model.mdlGeneral mdlBarcode = new model.mdlGeneral();
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM historytransaction WHERE IdHistory = ?");
			jrs.setString(1,param.getIdTransaction());
		
			jrs.execute();
			
			
			
			
			while(jrs.next()){
				mdlBarcode.setIdTransaction(param.getIdTransaction());
				mdlBarcode.setBarcode(param.getIdTransaction() + jrs.getString("Username") + param.getActualAmount()+ adapter.VariableAdapter.ChangeDateFormat("yyyy-M-dd hh:mm:ss", "yyyyMddhhmmss", jrs.getString("Date")));
				mdlBarcode.setActualAmount(param.getActualAmount());
			}
		
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("ActualNominal",param.getActualAmount());
			jrs.updateString("Barcode", mdlBarcode.getBarcode());
			jrs.updateRow();
			jrs.close();
			
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return mdlBarcode;
	}

	
	
	public static void SendBarcodeToClient(model.mdlGeneral param){
		
		
		String message = "This Is Your Identification Number";
		String title = "CPBARCODE";
		
		List<String> listRegId = new ArrayList<String>();
		
		listRegId.add(adapter.TokenAdapter.GetClientTokenByIdTransaction(param.getIdTransaction()));
		
		
		model.mdlDataGeneral data = new model.mdlDataGeneral();
		data.setTitle(title);
		data.setIdTransaction(param.getIdTransaction());
		data.setActualAmount(param.getActualAmount());
		data.setBarcode(param.getBarcode());
		data.setListFractionalCurrency(param.getListFractionalCurrency());
		
		adapter.PushNotificationAdapter.SendNotificationGeneralToClient(message, title, listRegId, data);
	}
	
	public static String InsertFractionalCurrency(model.mdlGeneral param){
		String result = "";
		
		String IdTransaction = param.getIdTransaction();
		List<model.mdlFractionalCurrency> listFractionalCurrency = param.getListFractionalCurrency();
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM transactionfractionalcurrency LIMIT 1");
			jrs.execute();
			
			
			
			for(model.mdlFractionalCurrency temp : listFractionalCurrency){
				jrs.moveToInsertRow();
				jrs.updateString("IdTransaction",IdTransaction);
				jrs.updateString("CurrencyName", temp.getCurrencyName());
				jrs.updateString("Quantity", temp.getQuantity());
				jrs.insertRow();

			}
				
			
			
			
		
			
			result = "1";
		}catch(SQLException ex){
			result = "0";
		}catch(ClassNotFoundException ex){
			result = "0";
		}
		
		return result;
	}
	
	

}
