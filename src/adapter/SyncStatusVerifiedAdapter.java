package adapter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

public class SyncStatusVerifiedAdapter {
	
	public static String getIdClientByIdTransaction(String idTransaction){
		String idClient ="";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT Username FROM historytransaction WHERE IdHistory = ?");
			jrs.setString(1,idTransaction);
		
			jrs.execute();
			
			
			while(jrs.next()){
				idClient = jrs.getString("Username");
			}
			
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return idClient;
		
	}
	public static String getIdPickerByIdTransaction(String idTransaction){
		String idPicker ="";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT IdPicker FROM orderpicker WHERE IdHistory = ?");
			jrs.setString(1,idTransaction);
		
			jrs.execute();
			
			
			while(jrs.next()){
				idPicker = jrs.getString("IdPicker");
			}
			
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return idPicker;
	}
	
	public static String UpdateStatus(model.mdlGeneral param){
		String result = "";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM historytransaction where IdHistory = ?");
			jrs.setString(1,param.getIdTransaction());
		
			jrs.execute();
			
		
			jrs.last();
			int rowNum = jrs.getRow();
			
			jrs.absolute(rowNum);
			jrs.updateString("Status", param.getStatus());
			jrs.updateRow();
			jrs.close();
			result ="1";
				
		
			
			
			
			
			
			
			
		}catch(SQLException ex){
			result = "0";
		}catch(ClassNotFoundException ex){
			result = "0";
		}
		
		return result;
	}
	
	public static void SendStatusToPicker(model.mdlGeneral param){
		
		String message = "Your Transaction Has Been Verified.";
		String title = "CPVERIFIED";
		
		List<String> listRegId = new ArrayList<String>();
		
		listRegId.add(adapter.TokenAdapter.GetPickerTokenByIdTransaction(param.getIdTransaction()));
		
		
		model.mdlDataGeneral data = new model.mdlDataGeneral();
		data.setTitle(title);
		data.setIdTransaction(param.getIdTransaction());
		data.setStatus(param.getStatus());
		
		adapter.PushNotificationAdapter.SendNotificationGeneralToPicker(message, title, listRegId, data);
	}
	
	
}
