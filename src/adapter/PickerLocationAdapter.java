package adapter;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.mdlPickerLocation;

public class PickerLocationAdapter {
	
	public static String InsertPickerLocation(model.mdlPickerLocation param){
		
		String result = "";
		
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM pickerlocation LIMIT 1");
			jrs.execute();
			
			jrs.moveToInsertRow();
			jrs.updateString("IdTransaction",param.getIdTransaction());
			jrs.updateString("IdPicker",param.getIdPicker());
			jrs.updateString("Date",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			jrs.updateString("Longitude",param.getLongitude());
			jrs.updateString("Latitude",param.getLatitude());
			
			jrs.insertRow();
			
			result = "1";
		}catch(SQLException ex){
			result = "0";
		}catch(ClassNotFoundException ex){
			result = "0";
		}
		
		return result;
	}
	
	public static model.mdlPickerLocation GetPickerLocation(model.mdlPickerLocation param){
		
	
		model.mdlPickerLocation mdlPickerLocation = new model.mdlPickerLocation();
		try
		{
		
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM pickerlocation WHERE IdTransaction = ? AND IdPicker = ?");
			jrs.setString(1, param.getIdTransaction());
			jrs.setString(2, param.getIdPicker());
			jrs.execute();
	
			
			jrs.beforeFirst();
			
			
			while(jrs.next()){
				
				
				mdlPickerLocation.setDate(jrs.getString("Date"));
				mdlPickerLocation.setIdPicker(jrs.getString("IdPicker"));
				mdlPickerLocation.setIdTransaction(jrs.getString("IdTransaction"));
				mdlPickerLocation.setLongitude(jrs.getString("Longitude"));
				mdlPickerLocation.setLatitude(jrs.getString("Latitude"));


			}
			
			jrs.close();
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return mdlPickerLocation;
	}

}
