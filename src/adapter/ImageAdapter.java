package adapter;

import java.sql.SQLException;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

public class ImageAdapter {
	
	public static model.mdlGetImageRes GetImage(model.mdlGetImageReq param){
		
		model.mdlGetImageRes mdlImage = new model.mdlGetImageRes();
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT b.Image FROM orderpicker a INNER JOIN employee b ON a.IdPicker = b.Username WHERE a.IdHistory = ?");
			jrs.setString(1,param.getIdTransaction());
		
			jrs.execute();
			
			int intRow = jrs.getRow();
			
			
			
			while(jrs.next()){
				mdlImage.setImage(jrs.getString("Image"));
			}
			
			
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return mdlImage;
	}

}
