package adapter;

import java.sql.SQLException;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

public class TokenAdapter {
	public static String GetClientTokenByIdTransaction(String idHistory){
		
		String Token = "";
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("select Token from historytransaction a INNER JOIN `user` b ON a.Username = b.Username WHERE a.IdHistory = ?");
			jrs.setString(1, idHistory);
			jrs.execute();
			
			
			
			
			while(jrs.next()){
				
				
				Token = jrs.getString("Token");
	
			}
			
			
			
			jrs.close();
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return Token;
	}
	
public static String GetPickerTokenByIdTransaction(String idHistory){
		
		String Token = "";
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("select c.Token from historytransaction a INNER JOIN orderpicker b ON a.IdHistory = b.IdHistory INNER JOIN pickeruser c ON b.IdPicker = c.Username where a.IdHistory = ?");
			jrs.setString(1, idHistory);
			jrs.execute();
			
			
			
			
			while(jrs.next()){
				
				
				Token = jrs.getString("Token");
	
			}
			
			
			
			jrs.close();
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return Token;
	}

	
}
