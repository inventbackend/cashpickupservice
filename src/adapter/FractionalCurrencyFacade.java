package adapter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

public class FractionalCurrencyFacade {
	
	public static List<model.mdlGetCurrency> GetFractionalCurrency(){
		List<model.mdlGetCurrency> ListCurrency = new ArrayList<model.mdlGetCurrency>();
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM fractionalcurrency");

			jrs.execute();
			
			
			
			
			while(jrs.next()){
				model.mdlGetCurrency mdlCurrency = new model.mdlGetCurrency();
				mdlCurrency.setCurrencyName(jrs.getString("CurrencyName"));
				ListCurrency.add(mdlCurrency);
			}
	
			
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return ListCurrency;
	}

}
