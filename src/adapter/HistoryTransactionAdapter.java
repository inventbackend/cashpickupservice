package adapter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

public class HistoryTransactionAdapter {
	
	public static List<model.mdlHistoryTransaction> getHistoryTransaction (model.mdlHistoryTransaction mdlParam){
		
		List<model.mdlHistoryTransaction> listHistorytransaction = new ArrayList<model.mdlHistoryTransaction>();
		
		try
		{
		
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM historytransaction WHERE IdHistory = ?");
			jrs.setString(1, mdlParam.getIdHistory());
			jrs.execute();
	
			
			jrs.beforeFirst();
			while(jrs.next()){
				model.mdlHistoryTransaction mdlHistoryTransaction  = new model.mdlHistoryTransaction();
				mdlHistoryTransaction.setIdHistory(jrs.getString("IdHistory"));
				mdlHistoryTransaction.setCodeBank(jrs.getString("CodeBank"));
				mdlHistoryTransaction.setDate(jrs.getString("Date"));
				mdlHistoryTransaction.setAddress(jrs.getString("Address"));
				mdlHistoryTransaction.setAddressDetail(jrs.getString("AddressDetail"));
				mdlHistoryTransaction.setLatitude(jrs.getString("Latitude"));
				mdlHistoryTransaction.setLongitude(jrs.getString("Longitude"));
				mdlHistoryTransaction.setAccount(jrs.getString("Account"));
				mdlHistoryTransaction.setNominal(jrs.getString("Nominal"));
				mdlHistoryTransaction.setKeterangan(jrs.getString("Keterangan"));
				mdlHistoryTransaction.setStatus(jrs.getString("Status"));
				listHistorytransaction.add(mdlHistoryTransaction);
				
				
			}
			
			jrs.close();
		}catch(SQLException ex){
			System.out.println(ex);
		}catch(ClassNotFoundException ex){
			System.out.println(ex);
		}
		
		return listHistorytransaction;
		
		
		
	}

	
	public static String InsertHistoryTransaction(model.mdlHistoryTransaction param) {
		
		String result = "";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM historytransaction LIMIT 1");
			jrs.execute();
			
			jrs.moveToInsertRow();
			jrs.updateString("IdHistory",param.getIdHistory());
			jrs.updateString("Username", param.getUsername());
			jrs.updateString("CodeBank", param.getCodeBank());
			jrs.updateString("Date", param.getDate());
			jrs.updateString("Address", param.getAddress());
			jrs.updateString("AddressDetail", param.getAddressDetail());
			jrs.updateString("Latitude", param.getLatitude());
			jrs.updateString("Longitude", param.getLongitude());
			jrs.updateString("Account", param.getAccount());
			jrs.updateString("Nominal", param.getNominal());
			jrs.updateString("Keterangan", param.getKeterangan());
			jrs.updateString("Status", param.getStatus());
			
			jrs.insertRow();
			
			result = "1";
		}catch(SQLException ex){
			result = "0";
		}catch(ClassNotFoundException ex){
			result = "0";
		}
		
		return result;
		
		
	}

	public static String updateToken(String token,String username, String password){
		
		String result = "";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM User where Username = ? AND Password = ?");
			jrs.setString(1, username);
			jrs.setString(2, password);
			jrs.execute();
			
			jrs.last();
			int getRow = jrs.getRow();
			jrs.absolute(getRow);
			jrs.updateString("Token", token);
			jrs.updateRow();
			
			jrs.close();
			
			result = "1";
			
		}catch(SQLException ex){
			result = "0";
			
		}catch(ClassNotFoundException ex){
			result = "0";
		}
		
		return result;
	}

	public static String UpdatePhone(String phone,String username){
		String result = "";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM customer where Username = ? ");
			jrs.setString(1, username);
			jrs.execute();
			
			jrs.last();
			int getRow = jrs.getRow();
			jrs.absolute(getRow);
			jrs.updateString("Phone", phone);
			jrs.updateRow();
			
			jrs.close();
			
			result = "1";
			
		}catch(SQLException ex){
			result = "0";
			
		}catch(ClassNotFoundException ex){
			result = "0";
		}
		
		return result;
	}
	
	public static String SendStatusNotification(String username, String password,String status,String idHistory){
		
		String result = "";
		List<String> listToken = new ArrayList<String>();

		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT Token FROM User where Username = ? AND Password = ?");
			jrs.setString(1, username);
			jrs.setString(2, password);
			jrs.execute();
			
			jrs.beforeFirst();
			while(jrs.next()){
				listToken.add(jrs.getString("Token"));
			}
			
			jrs.close();
			
			String message = "Your Order is Queued";
			
			adapter.PushNotificationAdapter.SendNotification( message, "CPCASHPICKUP", listToken);
			adapter.HistoryTransactionAdapter.InsertClientNotificationLog(idHistory, username, "CPCASHPICKUP");
			
			
			result = "1";
			
		}catch(SQLException ex){
			result = "0";
			
		}catch(ClassNotFoundException ex){
			result = "0";
		}
		
		return result;
	}
	
public static void InsertClientNotificationLog(String idHistory, String Username,String title){
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM notificationlogclient LIMIT 1");
			jrs.execute();
			
			jrs.moveToInsertRow();
			
			jrs.updateString("IdHistory", idHistory);
			jrs.updateString("Title", title);
			jrs.updateInt("Sent", 1);
			jrs.updateInt("Recieved", 0);
			jrs.updateString("Username", Username);

			jrs.insertRow();
			
			jrs.close();
			
		}catch(SQLException ex){
		
		}catch(ClassNotFoundException ex){
		
		}
	}
}


