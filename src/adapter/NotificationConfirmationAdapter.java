package adapter;

import java.sql.SQLException;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

public class NotificationConfirmationAdapter {
	
	public static void InsertPickerNotificationLog(String idHistory, String idPicker,String title){
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM notificationlog LIMIT 1");
			jrs.execute();
			
			jrs.moveToInsertRow();
			
			jrs.updateString("IdHistory", idHistory);
			jrs.updateString("Title", title);
			jrs.updateInt("Sent", 1);
			jrs.updateInt("Recieved", 0);
			jrs.updateString("IdPicker", idPicker);

			jrs.insertRow();
			
			jrs.close();
			
		}catch(SQLException ex){
		
		}catch(ClassNotFoundException ex){
		
		}
	}
	
public static void InsertClientNotificationLog(String idHistory, String Username,String title){
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = Database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT * FROM notificationlogclient LIMIT 1");
			jrs.execute();
			
			jrs.moveToInsertRow();
			
			jrs.updateString("IdHistory", idHistory);
			jrs.updateString("Title", title);
			jrs.updateInt("Sent", 1);
			jrs.updateInt("Recieved", 0);
			jrs.updateString("Username", Username);

			jrs.insertRow();
			
			jrs.close();
			
		}catch(SQLException ex){
		
		}catch(ClassNotFoundException ex){
		
		}
	}



	
}
